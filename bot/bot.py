import telebot
from telebot import types
import requests
import random
import random, string
from sqlalchemy.orm import Session
from sqlalchemy import create_engine
from sqlalchemy import Column, ForeignKey, Integer, Table, String, DateTime, Sequence
from sqlalchemy.orm import declarative_base, relationship, Session
from sqlalchemy_utils import drop_database
from sqlalchemy.sql import func
from sqlalchemy.orm import backref


db_host = "194.87.232.20"
Base = declarative_base()
engine = create_engine(f'postgresql+psycopg://postgres:postgres@{db_host}:6666/trydb', echo=False)
token='5623499060:AAHyNVRlwBD5MQ3ld6vuyoniI800qfPvq4k'
bot = telebot.TeleBot(token);


class User(Base):
    __tablename__ = "users"
    tg_login = Column(String, primary_key=True)
    password = Column(String)
    discription = Column(String)
    def as_dict(self):
       return {c.name: getattr(self, c.name) for c in self.__table__.columns}


def check_user_exist(tg_login: str, password: str) -> bool:
    with Session(autoflush=False, bind=engine) as session:
        b = session.query(User).where(User.tg_login == tg_login and User.password == password)
    return len(b.all()) != 0


def add_user(tg_login: str, password: str):
    with Session(autoflush=False, bind=engine) as session:
        b = session.query(User).where(User.tg_login == tg_login).count()
        session.commit()
    print(b)
    if b != 0:
        return
    user = User(tg_login=tg_login, password=password)
    with Session(autoflush=False, bind=engine) as session:
        b = session.add(user)
        session.commit()


def generate_password():
    letters = 'qwertyuiopasdfghjklzxcvbnm'
    return ''.join(letters[random.randint(0, len(letters) - 1)] for i in range(7))


def save_photo(user_id, tg_login):
    url = f'https://api.telegram.org/bot{token}/getUserProfilePhotos'
    params = {
        'user_id': user_id
    }
    response = requests.get(url, params=params)
    data = response.json() 
    if data['ok']:
        file_id = data['result']['photos'][0][0]['file_id']
        url = f'https://api.telegram.org/bot{token}/getFile'
        params = {
            'file_id': file_id
        }
        response = requests.get(url, params=params)
        data = response.json()
        if data['ok']:
            file_path = data['result']['file_path']
            file_url = f'https://api.telegram.org/file/bot{token}/{file_path}'
            response = requests.get(file_url)
    
            with open(f'photo/{tg_login}.jpg', 'wb') as f:
                f.write(response.content)


def check_directory(path, file_name):
    t = '.jpg'
    files_list = os.listdir(path)
    for i in files_list:
        if i == file_name + t:
            return True
    return False          


@bot.message_handler(content_types='text')
def message_reply(message):
    tg_login = message.from_user.username
    tg_id = message.from_user.id
    with Session(autoflush=False, bind=engine) as session:
        b = session.query(User).where(User.tg_login == tg_login).count()
        session.commit()
    if b == 0:
        password = generate_password()
        bot.send_message(message.from_user.id, f"Твой код: {password}")
        add_user(tg_login, password)
        try:
            if not check_directory("/photo", tg_login):
                save_photo(tg_id, tg_login)
        except Exception as r:
            pass


bot.infinity_polling()
    





